INCLUDE 'emu8086.inc'
ORG 100H
Data :              
JMP MULAI             
    Awal         db "Program Penghitungan Luas Kubus ",13,10,'$'
    Input        db "Masukkan Panjang Sisi : $"
    Hasil        db ,13,10,"Hasil Luas Kubus Adalah : $"    
    Inputan     DW ?
    Banyak_sisi DW 6

MULAI:
LEA DX,Awal           
MOV AH,9              
INT 21H               

LEA DX,Input
MOV AH,9              
INT 21H

CALL SCAN_NUM         

MOV Inputan,CX            


MOV AX,Inputan            
MOV BX,Inputan            
MUL BX
MOV BX,Banyak_sisi
MUL BX 
PUSH AX

LEA DX,Hasil
MOV AH,9              
INT 21H         

POP AX

CALL PRINT_NUM        


DEFINE_SCAN_NUM       
DEFINE_PRINT_NUM
DEFINE_PRINT_NUM_UNS
END Data
